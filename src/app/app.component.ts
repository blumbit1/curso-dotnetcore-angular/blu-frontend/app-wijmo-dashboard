import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'wijmo-dashboard';

  selectedCountryName: string | undefined;

  countryName(e: any) {
    this.selectedCountryName = e;
  }
}
