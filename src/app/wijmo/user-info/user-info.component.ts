import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as wjCore from '@grapecity/wijmo';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnChanges{

  @Input('selectedCountryName')
  selectedCountryName!: string;
  selectedCountryData: wjCore.CollectionView;
  usersTooltip = 'Information on the last 200 users from selected country.';

  constructor(private dataService: DataService) {
    this.selectedCountryData = new wjCore.CollectionView(dataService['getCountryInfo']('United States'), {
      pageSize: 25
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['selectedCountryName'].currentValue && this.dataService.isValidCountry(changes['selectedCountryName'].currentValue)) {
      this.selectedCountryData = new wjCore.CollectionView(this.dataService['getCountryInfo'](changes['selectedCountryName'].currentValue), {
        pageSize: 25
      });
    }
  }
}
