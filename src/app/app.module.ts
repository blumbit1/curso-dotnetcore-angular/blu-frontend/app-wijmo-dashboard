import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Wijmo Modules
import { WjCoreModule } from '@grapecity/wijmo.angular2.core';
import { WjGridModule } from '@grapecity/wijmo.angular2.grid';
import { WjChartModule } from '@grapecity/wijmo.angular2.chart';
import { WjChartMapModule } from '@grapecity/wijmo.angular2.chart.map';
import { WjGaugeModule } from '@grapecity/wijmo.angular2.gauge';
import { WjInputModule } from '@grapecity/wijmo.angular2.input';
import { CurrentInfoComponent } from './wijmo/current-info/current-info.component';
import { SessionInfoComponent } from './wijmo/session-info/session-info.component';
import { TopPlatformInfoComponent } from './wijmo/top-platform-info/top-platform-info.component';
import { TopBrowserComponent } from './wijmo/top-browser/top-browser.component';
import { TopCountryInfoComponent } from './wijmo/top-country-info/top-country-info.component';
import { MapInfoComponent } from './wijmo/map-info/map-info.component';
import { UserInfoComponent } from './wijmo/user-info/user-info.component';
import { IssueInfoComponent } from './wijmo/issue-info/issue-info.component';

@NgModule({
  declarations: [
    AppComponent,
    CurrentInfoComponent,
    SessionInfoComponent,
    TopPlatformInfoComponent,
    TopBrowserComponent,
    TopCountryInfoComponent,
    MapInfoComponent,
    UserInfoComponent,
    IssueInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    WjCoreModule,
    WjGridModule,
    WjChartModule,
    WjChartMapModule,
    WjGaugeModule,
    WjInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
